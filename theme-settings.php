<?php

/**
 * @file
 * Drupal theme settings file.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function uw_fdsu_theme_resp_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['branding'] = array(
    '#type' => 'fieldset',
    '#title' => t('Branding options'),
  );
  $form['branding']['branding_level'] = array(
    '#type' => 'select',
    '#title' => t('Branding level:'),
    '#default_value' => variable_get('uw_theme_branding', 'full'),
    '#options' => array(
      'full' => t('Full University branding'),
      'generic' => t('Generic with University wordmark'),
      'generic_wordmark' => t('Generic with University wordmark only'),
      'generic_barebones' => t('Fully generic'),
    ),
  );
  $form['#submit'][] = 'uw_fdsu_theme_resp_set_branding';
}

/**
 * Form submit handler.
 */
function uw_fdsu_theme_resp_set_branding($form, &$form_state) {
  if (isset($form_state['values']['branding_level'])) {
    variable_set('uw_theme_branding', $form_state['values']['branding_level']);
  }
}
