<?php

/**
 * @file
 * Template for almost the entire page. Most of the page is inside div#site.
 */

$uw_theme_branding = variable_get('uw_theme_branding', 'full');
?>
<div class="breakpoint"></div>
<div id="site" class="<?php print $classes; ?> uw-site"<?php print $attributes; ?>>
  <div class="uw-site--inner">
    <div class="uw-section--inner">
      <nav id="skip" class="skip" aria-label="Skip links">
        <a href="#main" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
        <a href="#footer" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
      </nav>
    </div>
    <header id="header" class="uw-header--global">
      <?php
      $global_message = file_get_contents('https://uwaterloo.ca/global-message.html');
      if ($global_message) {
        ?>
        <div id="global-message">
          <?php echo $global_message; ?>
        </div>
        <?php
      }
      ?>
      <div class="uw-section--inner">
        <?php print render($page['global_header']); ?>
      </div>
    </header>
    <div id="site--offcanvas" class="uw-site--off-canvas <?php print ($uw_theme_branding === 'full') ? 'non_generic_header' : 'generic_header'; ?>">
      <div class="uw-section--inner">
        <?php print render($page['site_header']); ?>
      </div>
    </div>
    <?php if ($uw_theme_branding === 'full') { ?>
      <div id="site-colors" class="uw-site--colors">
        <div class="uw-section--inner">
          <div class="uw-site--cbar">
            <div class="uw-site--c1 uw-cbar"></div>
            <div class="uw-site--c2 uw-cbar"></div>
            <div class="uw-site--c3 uw-cbar"></div>
            <div class="uw-site--c4 uw-cbar"></div>
          </div>
        </div>
      </div>
    <?php } ?>
    <div id="site-header" class="uw-site--header" role="region" aria-label="Site title">
      <div class="uw-section--inner">
        <a href="<?php print $front_page ?>" title="<?php print $site_name; ?>" rel="home">
          <?php print $site_name; ?>
          <?php if (isset($site_slogan) && $site_slogan !== "") : ?>
            <span class="uw-site—subtitle"> <?php print $site_slogan; ?> </span>
          <?php endif; ?>
        </a>
      </div>
    </div>
    <div class="uw-header--banner__alt">
      <div class="uw-section--inner">
        <?php print render($page['banner_alt']); ?>
      </div><!-- /uw-headeranner__alt -->
    </div>
    <div id="main" class="uw-site--main" role="main">
      <div class="uw-section--inner">
        <nav id="site-navigation-wrapper" class="uw-site-navigation--wrapper" aria-label="Site">
          <div id="site-specific-nav" class="uw-site-navigation--specific">
            <div id="site-navigation" class="uw-site-navigation">
              <?php if (user_access('access content')) {
                print render($page['sidebar_first']);
              } ?>
            </div>
          </div>
        </nav>
        <div class="uw-site--main-top">
          <div class="uw-site--banner">
            <?php print render($page['banner']); ?>
          </div>
          <div class="uw-site--messages">
            <?php print $messages; ?>
          </div>
          <div class="uw-site--help">
            <?php print render($page['help']); ?>
          </div>
          <div class="uw-site--breadcrumb">
            <?php print $breadcrumb; ?>
          </div>
          <div class="uw-site--title">
            <?php list($title, $content) = uw_fdsu_theme_separate_h1_and_content($page, $title); ?>
            <h1><?php print $title ?: $site_name; ?></h1>
          </div>
        </div>
        <!-- when logged in -->
        <?php if ($tabs) : ?>
          <div class="node-tabs uw-site-admin--tabs"><?php print render($tabs); ?></div>
        <?php endif; ?>
        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <div class="uw-site-main--content">
          <div id="content" class="uw-site-content">
            <?php print $content; ?>
          </div><!--/main-content-->
          <?php $sidebar = render($page['sidebar_second']); ?>
          <?php $sidebar_promo = render($page['promo']); ?>
          <?php if (isset($sidebar) || isset($sidebar_promo)) { ?>
            <?php if (($sidebar !== NULL && $sidebar !== '') || ($sidebar_promo !== NULL && $sidebar_promo !== '')) { ?>
              <div id="site-sidebar-wrapper" class="uw-site-sidebar--wrapper">
                <div id="site-sidebar-nav" class="uw-site-sidebar--nav">
                  <div id="site-sidebar" class="uw-site-sidebar<?php echo ($sidebar_promo !== NULL && $sidebar_promo !== '') ? ' sticky-promo' : ''; ?>">
                    <?php if (isset($sidebar_promo)) { ?>
                      <?php if ($sidebar_promo !== NULL && $sidebar_promo !== '') { ?>
                        <div class="uw-site-sidebar--promo">
                          <?php print render($page['promo']); ?>
                        </div>
                      <?php } ?>
                    <?php } ?>
                    <div class="uw-site-sidebar--second">
                      <?php print render($page['sidebar_second']); ?>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
          <?php } ?>
        </div>
      </div><!--/section inner-->
    </div><!--/site main-->
    <div id="footer" class="uw-footer" role="contentinfo">
      <div id="uw-site-share" class="uw-site-share">
        <div class="uw-section--inner">
          <div class="uw-section-share"></div>
          <ul class="uw-site-share-top">
            <li class="uw-site-share--button__top">
              <a href="#main" id="uw-top-button" class="uw-top-button">
                <span class="ifdsu fdsu-arrow"></span>
                <span class="uw-footer-top-word">TOP</span>
              </a>
            </li>
            <li class="uw-site-share--button__share">
              <a href="#uw-site-share" class="uw-footer-social-button">
                <span class="ifdsu fdsu-share"></span>
                <span class="uw-footer-share-word">Share</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <?php include(__DIR__ . '/site-footer.tpl.php'); ?>
      <?php print render($page['global_footer']); ?>
    </div>
  </div>
</div><!--/site-->
<div class="ie-resize-fix"></div>
