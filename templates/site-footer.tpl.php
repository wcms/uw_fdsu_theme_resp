<?php

  /**
    * @file
    * Template for almost the entire page. Most of the page is inside div#site.
    */
?>
<div id="site-footer" class="uw-site-footer site-footer-toggle open-site-footer">
  <div class="uw-section--inner">
    <div class="uw-site-footer1<?php print empty($page['site_footer']) ? ' uw-no-site-footer' : ''; ?>">
      <div class="uw-site-footer1--logo-dept">
        <?php
          $site_logo_info = variable_get('uw_site_logo_info');
          $facebook = variable_get('uw_nav_site_footer_facebook');
          $twitter = variable_get('uw_nav_site_footer_twitter');
          $instagram = variable_get('uw_nav_site_footer_instagram');
          $youtube = variable_get('uw_nav_site_footer_youtube');
          $linkedin = variable_get('uw_nav_site_footer_linkedin');
          $snapchat = variable_get('uw_nav_site_footer_snapchat');
        ?>
        <?php if ($site_logo_info) : ?>
          <a href="<?php print $site_logo_info['link']; ?>"><img src="<?php print $site_logo_info['logo']; ?>" alt="<?php print $site_logo_info['alt']; ?>" /></a>
        <?php else : ?>
          <?php $site_name = (strtolower($site_name)); print '<a href="' . url('<front>') . '">' . (ucfirst($site_name)) . '</a>'; ?>
        <?php endif; ?>
      </div>
      <div class="uw-site-footer1--contact">
        <ul class="uw-footer-social">
          <?php if ($facebook !== "" && $facebook !== NULL): ?>
            <li>
              <a href="https://www.facebook.com/<?php print check_plain($facebook); ?>" aria-label="facebook">
                <span class="ifdsu fdsu-facebook"></span>
              </a>
            </li>
          <?php endif; ?>

          <?php if ($twitter !== "" && $twitter !== NULL): ?>
            <li>
              <a href="https://www.twitter.com/<?php print check_plain($twitter); ?>" aria-label="twitter">
                <span class="ifdsu fdsu-twitter"></span>
              </a>
            </li>
          <?php endif; ?>

          <?php if ($youtube !== "" && $youtube !== NULL): ?>
            <li>
              <a href="https://www.youtube.com/<?php check_plain(print $youtube); ?>" aria-label="youtube">
                <span class="ifdsu fdsu-youtube"></span>
              </a>
            </li>
          <?php endif; ?>

          <?php if ($instagram !== "" && $instagram !== NULL): ?>
            <li>
              <a href="https://www.instagram.com/<?php print check_plain($instagram); ?>" aria-label="instagram">
                <span class="ifdsu fdsu-instagram"></span>
              </a>
            </li>
          <?php endif; ?>

          <?php if ($linkedin !== "" && $linkedin !== NULL): ?>
            <li>
              <a href="https://www.linkedin.com/<?php check_plain(print $linkedin); ?>" aria-label="linkedin">
                <span class="ifdsu fdsu-linkedin"></span>
              </a>
            </li>
          <?php endif; ?>

          <?php if ($snapchat !== "" && $snapchat !== NULL): ?>
            <li>
              <a href="https://www.snapchat.com/add/<?php check_plain(print $snapchat); ?>" aria-label="snapchat">
                <span class="ifdsu fdsu-snapchat"></span>
              </a>
            </li>
          <?php endif; ?>
        </ul>
      </div>
    </div>
    <?php if (!empty($page['site_footer']['uw_nav_site_footer_site-footer']['#node']->field_body_no_summary)): ?>
      <div class="uw-site-footer2">
        <?php print render($page['site_footer']); ?>
      </div>
    <?php endif; ?>
  </div>
</div>
