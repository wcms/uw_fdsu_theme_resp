<?php

/**
 * @file
 * Add meta tags to enable modile view and head.
 */

/**
 * Implements hook_page_alter().
 */
function uw_fdsu_theme_resp_page_alter(&$page) {
  // Force the global footer region to display when empty.
  if (!isset($page['global_footer'])) {
    foreach (system_region_list($GLOBALS['theme'], REGIONS_ALL) as $region => $name) {
      if (in_array($region, array('global_footer'))) {
        $page['global_footer'] = array(
          '#region' => 'global_footer',
          '#weight' => '-10',
          '#theme_wrappers' => array('region'),
        );
      }
    }
  }
}

/**
 * Implements template_preprocess_block().
 */
function uw_fdsu_theme_resp_preprocess_block(&$variables) {
  // Add @aria-label to responsive_menu_combined block.
  if (!empty($variables['block']->bid) && $variables['block']->bid === 'responsive_menu_combined-responsive_menu_combined') {
    $variables['attributes_array']['aria-label'] = 'Main';
  }
}

/**
 * Add meta tags to enable mobile view.
 */
function uw_fdsu_theme_resp_preprocess_html(&$variables) {
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=edge',
    ),
  );
  $meta_mobile_view = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1',
    ),
  );

  $rwd_path = drupal_get_path('theme', 'uw_fdsu_theme_resp') . '/css/rwd.css';
  drupal_add_css($rwd_path);
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
  drupal_add_html_head($meta_mobile_view, 'meta_mobile_view');
}

/**
 * Implements hook_css_alter().
 */
function uw_fdsu_theme_resp_css_alter(&$css) {
  // uw_core_theme and uw_fdsu_theme are loaded, but we don't want their CSS.
  // Remove any CSS added by these.
  $path = drupal_get_path('theme', 'uw_core_theme') . '/';
  foreach ($css as $key => $source) {
    if (substr($key, 0, strlen($path)) == $path) {
      unset($css[$key]);
    }
  }
  $pathFDSU = drupal_get_path('theme', 'uw_fdsu_theme') . '/';
  foreach ($css as $key => $source) {
    if (substr($key, 0, strlen($pathFDSU)) == $pathFDSU) {
      unset($css[$key]);
    }
  }

  // Remove CSS added by various modules.
  $remove['ctools'][] = '/css/modal.css';
  $remove['uw_nav_global_footer'][] = '/css/uw_nav_global_footer.css';
  $remove['uw_nav_site_footer'][] = '/css/uw_nav_site_footer.css';
  $remove['system'][] = '/system.base.css';
  $remove['system'][] = '/system.theme.css';
  $remove['system'][] = '/system.menus.css';
  $remove['uw_ct_embedded_facts_and_figures'][] = '/css/highlighted_fact.css';
  $remove['uw_ct_embedded_call_to_action'][] = '/css/uw_ct_embedded_call_to_action.css';
  $remove['uw_ct_home_page_banner'][] = '/css/banner.css';
  $remove['uw_ct_home_page_banner'][] = '/css/uw_banner_slideshow.css';
  $remove['uw_ct_service'][] = '/css/uw_ct_service.css';
  $remove['uw_ct_blog'][] = '/css/uw_ct_blog.css';
  $remove['uw_ct_news_item'][] = '/css/uw_ct_news_item.css';
  $remove['uw_ct_event'][] = '/css/uw_ct_event.css';
  $remove['uw_ct_opportunities'][] = '/css/uw_ct_opportunities.css';
  $remove['uw_ct_person_profile'][] = '/css/uw_ct_person_profile.css';
  $remove['uw_social_media_sharing'][] = '/css/uw_social_media_sharing.css';
  $remove['date'][] = '/date_api/date.css';
  $remove['date'][] = '/date_views/css/date_views.css';
  $remove['uw_ct_contact'][] = '/css/uw_ct_contact.css';
  $remove['views'][] = '/css/views.css';
  $exclude = array();
  foreach ($remove as $module => $items) {
    if (module_exists($module)) {
      $module_path = drupal_get_path('module', $module);
      foreach ($items as $exclude_item) {
        $exclude[$module_path . $exclude_item] = TRUE;
      }
    }
  }
  $css = array_diff_key($css, $exclude);
}

/**
 * Implements hook_js_alter().
 */
function uw_fdsu_theme_resp_js_alter(&$javascript) {
  if (module_exists('uw_header_search')) {
    $rwdjs_path = drupal_get_path('module', 'uw_header_search') . '/uw_header_search.js';
    drupal_add_js($rwdjs_path);
    // drupal_add_js($tabsjs_path);
    drupal_add_js(array(
      'CToolsModal' => array(
        'modalSize' => array(
          'type' => 'scale',
          'width' => 1,
          'height' => 1,
        ),
        'modalOptions' => array(
          'opacity' => .98,
          'background-color' => '#252525',
        ),
        'animation' => 'fadeIn',
        'animationSpeed' => 'slow',
        'throbberTheme' => 'CToolsThrobber',
        // Customize the AJAX throbber like so: This function assumes the images
        // are inside the module directory's "images" directory:
        'throbber' => theme('image', array(
          'path' => ctools_image_path('spacer.png', 'uw_header_search'),
          'alt' => t('Loading...'),
          'title' => t('Loading'),
        )),
        'closeText' => '',
        'closeImage' => theme('image', array(
          'path' => ctools_image_path('spacer.png', 'uw_header_search'),
          'alt' => '',
          'title' => '',
        )),
      ),
    ), 'setting');
  }
}
