# UW FDSU theme responsive (uw_fdsu_theme_resp)

The homepage theme using a combination of:
* [Node](https://www.npmjs.com/)
    * currently version 12.16.1
* [gulp](https://gulpjs.com/)
    * currently version 4.0.2

## Installing tools to compile theme

To install the tools required to compile the theme, ensure that you are in the theme (uw_home_theme) directory:

    npm install

Once all the modules are installed, and you are still in theme directory, compile the files by running:

    gulp

This will also watch the directory for any changes and re-compile as soon as a change is detected.